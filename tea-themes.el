;;; tea-themes.el --- A custom theme  -*- lexical-binding: t; -*-
;; Copyright (C) 2024 Tilman Andre Mix
;; -------------------------------------------------------------------
;; Authors: Tilman Andre Mix
;; -------------------------------------------------------------------
;; URL: https://codeberg.org/tilmanmixyz/tea-themes
;; -------------------------------------------------------------------
;; Created: 2021-03-16
;; Version: 0.1
;; Package-Requires: ((emacs "21.1"))
;; -------------------------------------------------------------------
;; This file is not part of GNU Emacs.
;;
;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the BSD Zero Clause License
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <https://opensource.org/license/0bsd/>
;; -------------------------------------------------------------------
;;; Commentary:
;; Tea-Theme provides a simple theme for GNU EMacs
;; -------------------------------------------------------------------

;;; Code:

;;;; Requirements
(unless (>= emacs-major-version 21)
  (error "Required GNU Emacs major version 25 or later"))

(defcustom tea-themes-italic-comments t
  "If t, use italics for comments."
  :type 'boolean
  :group 'tea-themes)

(defcustom tea-themes-custom-colors nil
  "Set custom colors for the-themes."
  :type 'alist
  :group 'tea-themes)

;;; Define Tea Faces:
(defgroup tea-themes nil
  "Faces and colors for tea-themes."
  :group 'faces)

(defface tea-bg nil "Background face for tea-themes." :group 'faces)
(defface tea-fg nil "Foreground face for tea-themes." :group 'faces)
(defface tea-black nil "Black face for tea-themes." :group 'faces)
(defface tea-white nil "White face for tea-themes." :group 'faces)
(defface tea-red nil "Red face for tea-themes." :group 'faces)
(defface tea-green nil "Green face for tea-themes." :group 'faces)
(defface tea-yellow nil "Yellow face for tea-themes." :group 'faces)
(defface tea-blue nil "Blue face for tea-themes." :group 'faces)
(defface tea-magenta nil "Magenta face for tea-themes." :group 'faces)
(defface tea-cyan nil "Cyan face for tea-themes." :group 'faces)
(defface tea-orange nil "Orange face for tea-themes." :group 'faces)
(defface tea-bright-black nil "Bright-Black face for tea-themes." :group 'faces)
(defface tea-bright-wihte nil "Bright-Wihte face for tea-themes." :group 'faces)
(defface tea-bright-red nil "Bright-Red face for tea-themes." :group 'faces)
(defface tea-bright-green nil "Bright-Green face for tea-themes." :group 'faces)
(defface tea-bright-yellow nil "Bright-Yellow face for tea-themes." :group 'faces)
(defface tea-bright-blue nil "Bright-Blue face for tea-themes." :group 'faces)
(defface tea-bright-magenta nil "Bright-Magenta face for tea-themes." :group 'faces)
(defface tea-bright-cyan nil "Bright-Cyan face for tea-themes." :group 'faces)
(defface tea-bright-orange nil "Bright-Orange face for tea-themes." :group 'faces)

(defun tea-themes-create (variant theme-name)
  "Define theme colors for VARIANT and THEME-NAME."
  (let ((class '((class color) (min-colors 89)))
        (tea-bg (cond ((eq variant 'dark) "#282828") ((eq variant 'light) "#ffffff")))
        (tea-fg (cond ((eq variant 'dark) "#ebdbb2") ((eq variant 'light) "#4d4d4c")))
        ;; Accent
        (tea-base0 (cond ((eq variant 'dark) "#0d1011") ((eq variant 'light) "#f2f2f2")))
        (tea-base1 (cond ((eq variant 'dark) "#1d2021") ((eq variant 'light) "#e4e4e4")))
        (tea-base2 (cond ((eq variant 'dark) "#282828") ((eq variant 'light) "#dedede")))
        (tea-base3 (cond ((eq variant 'dark) "#3c3886") ((eq variant 'light) "#d6d4d4")))
        (tea-base4 (cond ((eq variant 'dark) "#665c54") ((eq variant 'light) "#c0bfbf")))
        (tea-base5 (cond ((eq variant 'dark) "#7c6f64") ((eq variant 'light) "#a3a1a1")))
        (tea-base6 (cond ((eq variant 'dark) "#928374") ((eq variant 'light) "#8a8787")))
        (tea-base7 (cond ((eq variant 'dark) "#d5c4a1") ((eq variant 'light) "#696769")))
        (tea-base8 (cond ((eq variant 'dark) "#fbf1c7") ((eq variant 'light) "#000000")))
        ;; Colors
        (tea-black              (cond ((eq variant 'dark) "#282828") ((eq variant 'light) "#1d1f21")))
        (tea-white              (cond ((eq variant 'dark) "#a89984") ((eq variant 'light) "#d6d6d6")))
        (tea-red                (cond ((eq variant 'dark) "#CC241d") ((eq variant 'light) "#c82829")))
        (tea-green              (cond ((eq variant 'dark) "#98971a") ((eq variant 'light) "#718c00")))
        (tea-yellow             (cond ((eq variant 'dark) "#d79921") ((eq variant 'light) "#f5871f")))
        (tea-blue               (cond ((eq variant 'dark) "#458588") ((eq variant 'light) "#4271ae")))
        (tea-magenta            (cond ((eq variant 'dark) "#b16286") ((eq variant 'light) "#8959a8")))
        (tea-cyan               (cond ((eq variant 'dark) "#689d6a") ((eq variant 'light) "#3e000f")))
        (tea-orange             (cond ((eq variant 'dark) "#d65d0e") ((eq variant 'light) "#f5871f")))
        (tea-bright-black       (cond ((eq variant 'dark) "#928374") ((eq variant 'light) "#8e908c")))
        (tea-bright-white       (cond ((eq variant 'dark) "#ebdbb2") ((eq variant 'light) "#efefef")))
        (tea-bright-red         (cond ((eq variant 'dark) "#fb4934") ((eq variant 'light) "#ff3334")))
        (tea-bright-green       (cond ((eq variant 'dark) "#b8bb26") ((eq variant 'light) "#89aa00")))
        (tea-bright-yellow      (cond ((eq variant 'dark) "#fabd2f") ((eq variant 'light) "#eab700")))
        (tea-bright-blue        (cond ((eq variant 'dark) "#83a598") ((eq variant 'light) "#5795e6")))
        (tea-bright-magenta     (cond ((eq variant 'dark) "#d3869b") ((eq variant 'light) "#b777e0")))
        (tea-bright-cyan        (cond ((eq variant 'dark) "#8ec07c") ((eq variant 'light) "#66bdc3")))
        (tea-bright-orange      (cond ((eq variant 'dark) "#fe8019") ((eq variant 'light) "#f5871f")))
        )

    (dolist (pair tea-themes-custom-colors)
      (let ((cvar (cdr pair))
            (val (car pair)))
        (set cvar val)))

    (custom-theme-set-faces
     theme-name
     `(default ((,class (:background ,tea-bg :foreground ,tea-fg))))
     `(cursor ((,class (:background ,tea-fg))))
     `(fringe ((,class (:inherit default :foreground ,tea-base4))))
     `(shadow ((,class (:foreground ,tea-base5))))

     ;; Line numbers
     `(line-number ((,class (:foreground ,tea-base5 :slant italic :inherit default))))
     `(line-number-current-line ((,class (:inherit line-number :weight bold :foreground ,tea-base8))))

     ;; Compilation
     `(error ((,class (:foreground ,tea-red))))
     `(warning ((,class (:foreground ,tea-yellow))))
     `(success ((,class (:foreground ,tea-green))))

     ;; Fontlock
     `(font-lock-builtin-face ((,class (:foreground ,tea-orange))))
     `(font-lock-comment-face ((,class (:foreground ,tea-bright-black))))
     `(font-lock-doc-face ((,class (:foreground ,tea-base8))))
     `(font-lock-constant-face ((,class (:foreground ,tea-magenta))))
     `(font-lock-function-name-face ((,class (:foreground ,tea-green))))
     `(font-lock-keyword-face ((,class (:foreground ,tea-red ))))
     `(font-lock-operator-face ((,class (:foreground ,tea-fg))))
     `(font-lock-type-face ((,class (:foreground ,tea-yellow))))
     `(font-lock-string-face ((,class (:foreground ,tea-green))))
     `(font-lock-variable-name-face ((,class (:foreground ,tea-blue))))
     `(font-lock-number-face ((,class (:foreground ,tea-magenta))))

     ;; Terminal
     `(term-color-black          ((,class (:background ,tea-black          :foreground ,tea-black))))
     `(term-color-red            ((,class (:background ,tea-red            :foreground ,tea-red))))
     `(term-color-green          ((,class (:background ,tea-green          :foreground ,tea-green))))
     `(term-color-yellow         ((,class (:background ,tea-yellow         :foreground ,tea-yellow))))
     `(term-color-blue           ((,class (:background ,tea-blue           :foreground ,tea-blue))))
     `(term-color-magenta        ((,class (:background ,tea-magenta        :foreground ,tea-magenta))))
     `(term-color-cyan           ((,class (:background ,tea-cyan           :foreground ,tea-cyan))))
     `(term-color-white          ((,class (:background ,tea-white          :foreground ,tea-white))))
     `(term-color-bright-black   ((,class (:background ,tea-bright-black   :foreground ,tea-bright-black))))
     `(term-color-bright-red     ((,class (:background ,tea-bright-red     :foreground ,tea-bright-red))))
     `(term-color-bright-green   ((,class (:background ,tea-bright-green   :foreground ,tea-bright-green))))
     `(term-color-bright-yellow  ((,class (:background ,tea-bright-yellow  :foreground ,tea-bright-yellow))))
     `(term-color-bright-blue    ((,class (:background ,tea-bright-blue    :foreground ,tea-bright-blue))))
     `(term-color-bright-magenta ((,class (:background ,tea-bright-magenta :foreground ,tea-bright-magenta))))
     `(term-color-bright-cyan    ((,class (:background ,tea-bright-cyan    :foreground ,tea-bright-cyan))))
     `(term-color-bright-white   ((,class (:background ,tea-bright-white   :foreground ,tea-bright-white))))
    )))

;;; Provide file
;;;###autoload
(when (and (boundp 'custom-theme-load-path) load-file-name)
  (let* ((base (file-name-directory load-file-name))
         (dir (expand-file-name "themes/" base)))
    (add-to-list 'custom-theme-load-path
                 (or (and (file-directory-p dir) dir)
                     base))))

(provide 'tea-themes)
;;; tea-themes.el ends here
